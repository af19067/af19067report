import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;

public class assignment {
    private JPanel yen;
    private JButton tempuraButton;
    private JButton udonButton;
    private JButton karaageButton;
    private JButton yakinikuButton;
    private JButton gyozaButton;
    private JButton ramenButton;
    private JButton checkOutButton;
    private JTextPane textPane1;
    private JLabel price;

    int total = 0;

    void order(String food){//メソッド
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order" + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation == 0){//はいを押した
            JOptionPane.showMessageDialog(null,
                    "Thank you for ordering " + food + "! It will be server as soon as possible");
            textPane1.setText(textPane1.getText() + food + "\n");
            total += 100;
            price.setText(""+total);
        }

    }
    public assignment() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Tempura");
            }
        });
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Karaage");
            }
        });

        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Gyoza");
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Udon");
            }
        });
        yakinikuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Yakiniku");
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Ramen");
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if(confirmation == 0){//はいを押した
                    JOptionPane.showMessageDialog(null,
                            "Thank you. The total price is "+total+"yen.");
                    textPane1.setText("");
                    total = 0;
                    price.setText(""+total);

                }

            }
        });
    }

    public static void main(String[] args) {
        int total = 0;
        JFrame frame = new JFrame("assignment");
        frame.setContentPane(new assignment().yen);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
